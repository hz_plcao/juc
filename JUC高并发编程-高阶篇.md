## 1. 基础知识复习

### 1.1 Java 多线程相关概念

- 1 把锁：synchronized

- 2 个并：

  - 并发（concurrent）：
    - 是在同一实体上的多个事件；
    - 是在一台处理器上“同时”处理多个任务；
    - 同一时刻，其实只有一个事件在发生
    - 举例：12306 抢票、秒杀
  - 并行（parallel）：
    - 是在不同实体上的多个事件
    - 是在多台处理器上同时处理多个任务
    - 同一时刻，大家真的都在做事情，你做你的，我做我的，但是我们都在做

  并发VS并行：

  ![image-20230221124221039](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221124221039.png)

- 3个程：

  - 进程：简单的说，在系统中运行的一个应用程序就是一个进程，每一个进程都有它自己的内存空间和系统资源
  - 线程：也被称为轻量级进程，在同一个进程内会有1个或多个线程，是大多数操作系统进行时序调度的基本单位
  - 管程：Monitor(监视器)，其实是一种同步机制，他的义务是保证（同一时间）只有一个线程可以访问被保护的数据和代码。也就是我们平时所说的锁。

- 用户线程和守护线程

  - Java线程分为用户线程和守护线程，一般情况下不做特别说明配置，==默认都是用户线程==

    - 用户线程：是系统的工作线程，他会完成这个程序需要完成的业务操作

    - 守护线程：是一种特殊的线程为其它线程服务的，在后台默默地完成一些系统性的服务，比如垃圾回收线程就是最典型的例子

      守护线程作为一个服务线程，没有服务对象就没有必要继续运行了，如果用户线程全部结束了，意味着程序需要完成的业务操作已经结束了，系统可以退出了。所以假如当系统只剩下守护线程的时候，Java虚拟机会自动退出。

```java
public class DeamonDemo {
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"\t 开始行动，"+(Thread.currentThread().isDaemon()?"守护线程":"用户线程"));
            while (true){

            }
        },"t1");
        // 将t1设置为守护线程
        //t1.setDaemon(true);
        t1.start();
        // 暂停几秒钟线程
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"\t ----end 主线程");
    }
}
```

演示结果：

![image-20230221134355563](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221134355563.png)

可以看到，t1、main 线程都是用户线程，而且程序还在运行

- 线程的**deamon**属性	
  - true表示是守护线程
  - flase表示是用户线程

![image-20230221134320363](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221134320363.png)

当我们将 t1 的Deamon值设置为true时，

![image-20230221134543200](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221134543200.png)

t1 变成了守护线程，main线程结束后，t1也结束了，从而证明了用户线程结束后，守护线程也会默默的结束。

### 1.2 总结

如果用户线程全部结束意味着程序需要完成的业务操作已经结束了，守护线程随着 JVM 一同结束工作。

注意：`setDeamon(true)` 方法必须在 start() 之前设置，否则报 IllegalThreadStateException 异常

## 2. Future 接口理论知识复习

Future 接口(FutureTask实现类)定义了操作 ***异步任务*** 执行一些方法，如获取异步任务的执行结果、取消任务的执行、判断任务是否被取消、判断任务执行是否完毕等。

比如 **主线程** 让一个 **子线程** 去执行任务，子线程可能比较耗时，启动子线程开始执行任务后，主线程就去做其他事情了，忙其他事情或者先执行完，过了一会才去获取子任务的执行结果或变更的任务状态。

案例：上课买水

老师上课讲课时想喝水，但是不能中断讲课，老师继续讲课（主线程），让班长去买水（子线程），买完水给老师。

==一句话：Future 接口可以为主线程开一个分支任务，专门为主线程处理耗时和费力的复杂业务。==

### 2.1 Future常用接口 FutureTask异步任务

能干什么

Future是JDK5新加的一个接口，他提供了一种**异步并行计算的功能**。

如果主线程需要执行一个很耗时的计算任务，我们就可以通过 future 把这个任务放到异步线程中执行。

主线程继续处理其他任务或者先行结束，在通过Future获取计算结果。



==目的：异步多线程任务执行且有返回结果，三个特点：多线程/有返回/异步任务==

（班长为老师去买水作为新启动的异步多线程任务且买到水有返回结果）

![image-20230221144037883](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221144037883.png)

```java
public class CompletableFutureDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<String> futureTask = new FutureTask<>(new MyThread());
        Thread t1 = new Thread(futureTask, "t1");

        t1.start();
        System.out.println(futureTask.get());
    }
}

class MyThread implements Callable<String> {

    @Override
    public String call() throws Exception {
        System.out.println("------come in call()");
        return "hello Callable";
    }
}
```

### 2.2 Future 优缺点

==优点：future + 线程池异步多线任务配合，能显著提高程序的执行效率==

案例：一个类FutureThreadPoolDemo中 有两个方法 m1(),m2(),其中 m1 未使用线程池，m2 使用了线程池

```java
public class FutureThreadPoolDemo {
    public static void main(String[] args) {
        // 未使用线程池
        m1();
        // 使用线程池
        m2();
    }

    public static void m2() {
        // 创建线程池
        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        long startTime = System.currentTimeMillis();
        // 3个任务，目前开启异步多线程任务来处理，请问耗时多少？

        // 直线程
        FutureTask<String> futureTask1 = new FutureTask(() -> {
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "task1 over";
        });
        threadPool.submit(futureTask1);

        // 支线程
        FutureTask<String> futureTask2 = new FutureTask(() -> {
            try {
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "task2 over";
        });
        threadPool.submit(futureTask2);

        // 主线程耗时
        try {
            TimeUnit.MILLISECONDS.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        System.out.println("m2 costTime " + (endTime - startTime) + " 毫秒");
        System.out.println(Thread.currentThread().getName() + "\t ---end");

        // 关闭线程池
        threadPool.shutdown();
    }

    public static void m1() {
        // 3个任务，目前只有一个线程main来处理，请问耗时多少？
        long startTime = System.currentTimeMillis();

        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            TimeUnit.MILLISECONDS.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            TimeUnit.MILLISECONDS.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();

        System.out.println("m1 costTime:" + (endTime - startTime) + " 毫秒");

        System.out.println(Thread.currentThread().getName() + "\t -----end");
    }
}
```

![image-20230221153823676](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221153823676.png)

很明显，使用了线程池要比不使用线程池节约将近1/3的时间。

缺点：

- get() 阻塞：一旦调用get() 方法求结果，如果计算没有完成容易导致程序阻塞

```java
public class FutureAPIDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<String> futureTask = new FutureTask<>(() -> {
            System.out.println(Thread.currentThread().getName()+"\t ----come in");
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "task over";
        });
        Thread t1 = new Thread(futureTask,"t1");
        t1.start();
        // get容易导致阻塞，一般建议放在程序后面，一旦调用不见不散， 非要等到结果才离开，不管你是否计算完成，容易造成程序堵塞。
        // 加入我不愿意等待很长时间，我希望过时不候，可以自动离开
        System.out.println(futureTask.get());
        System.out.println(Thread.currentThread().getName()+"\t ---其他忙完了");
    }
}
```

![image-20230221161907129](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221161907129.png)

- isDone() 轮询 ：轮询的方式会耗费无谓的CPU资源，而且也不见得能及时地得到计算结果。

  如果想要异步获取结果，通常都会以轮询的方式去获取结果，尽量不要阻塞。

```java
public class FutureAPIDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<String> futureTask = new FutureTask<>(() -> {
            System.out.println(Thread.currentThread().getName()+"\t ----come in");
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "task over";
        });
        Thread t1 = new Thread(futureTask,"t1");
        t1.start();

        System.out.println(Thread.currentThread().getName()+"\t ---忙其他的了");

        // 轮询
        while (true){
            if(futureTask.isDone()){
                System.out.println(futureTask.get());
                break;
            }else{
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("正在处理中，不要在催了，再催熄火");
            }
        }
    }
}
```

![image-20230221163205841](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221163205841.png)

==**结论：**==

Future 对于结果的获取方式不是很友好，只能通过阻塞或轮询的方式得到任务的结果。



对于一些简单的业务场景使用Future完全OK，那么对于复杂的任务呢？

### 2.3 完成一些复杂的任务的诉求

1. 回调通知：

   应对 Future 的完成时间，完成了可以告诉我，也就是我们的回调通知通过轮询的方式去判断任务是否完成这样非常占CPU并且代码也不优雅

2. 创建异步任务

   Future+线程池配合

3. 多个任务前后依赖可以组合处理（水煮鱼）

   想将多个异步任务的计算结果组合起来，后一个异步任务的计算结果需要前一个异步任务的值，将两个或多个异步计算合成一个异步计算，这几个异步计算互相独立，同时后面这个有依赖前一个处理的结果。

4. 当Future集合中某个任务最快结束时，返回结果，返回第一名处理结果。

使用Future之前提供的那点API就囊中羞涩，处理起来不够优雅，这时候还是让CompletableFuture以声明式的方式优雅的处理这些需求，Future能干的CompletableFuture都能干



## 3. CompletableFuture对Future的改进

### 3.1 CompletableFuture为什么出现

get()方法在Future在计算完成之前会一直处在阻塞状态下

isDone() 方法容易耗费CPU资源

对于真正的异步处理我们希望是可以通过传入回调函数，在Future结束时自动调用该回调函数，这样，我们就不用等待结果。

==阻塞的方式和异步编程的设计理念相违背，而轮询的方式会耗费无谓的CPU资源==。因此，JDK8设计出CompletableFuture。

CompletableFuture提供了一种观察者模式类似的机制，可以让人物执行完成后通知监听的一方。

### 3.2CompletableFuture和CompletionStage源码分别介绍

**类架构说明：**

![image-20230221171656759](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221171656759.png)

![image-20230221171710215](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221171710215.png)

#### 3.2.1 接口 CompletionStage：

```markdown
CompletionStage 代表异步计算过程的某一个阶段，一个阶段完成以后可能会触发另外一个阶段

一个阶段的计算执行可以是一个Function，Consumer或者Runnable。比如：stage.thenApply(x -> square(x) ).thenAccept(x -> System.out.print(x)).thenRun(() -> System.out.print())

一个阶段的执行可能是被单个阶段的完成触发，也可能是由多个阶段一起触发
```

==一句话：代表异步计算过程中的某一个阶段，一个阶段完成以后可能会触发另外一个阶段，有些类似Linux系统的管道分隔符传参数。==

#### 3.2.2 CompletableFuture类：

```java
	在Java8中，CompletableFuture提供了非常强大的扩展功能，可以帮助我们简化异步编程的复杂性，并且提供了函数式编程的能力，可以通过回调的方式处理计算结果，也提供了转换和组合 CompletableFuture 的方法
    它可能代表一个明确完成的Future，也有可能代表一个完成阶段（CompletionStage）,他支持在计算完成以后出发一些函数或执行某些动作
    它实现了 Future 和 CompletionStage接口
```

#### 3.2.3 核心4个静态方法，来创建一个异步任务

可以通过new的方式创建，但是官方不见以使用。

```java
CompletableFuture completableFuture = new CompletableFuture();
```

官方建议使用以下方式：

##### runAsync 无返回值

```java
 /**
     *
     * @param runnable the action to run before completing the returned CompletableFuture
     * @return the new CompletableFuture
     */
    public static CompletableFuture<Void> runAsync(Runnable runnable)


    /**
     *
     * @param runnable the action to run before completing the returned CompletableFuture
     * @param executor the executor to use for asynchronous execution 
     * @return the new CompletableFuture
     */
    public static CompletableFuture<Void> runAsync(Runnable runnable, Executor executor)
```

案例：

未指定线程池

![image-20230221204428292](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221204428292.png)

![image-20230221181545546](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221181545546.png)

指定线程池：

![image-20230221203137832](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221203137832.png)

![image-20230221203155105](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221203155105.png)

所以说，没有指定 Executor 的方法，直接使用默认的ForkJoinPool.commonPool() 作为它的线程池执行异步代码。

如果指定线程池，则使用我们自定义的或者特别指定的线程池执行异步代码

##### supplyAsync 有返回值

```java
/**
  * @param supplier a function returning the value to be used
  * to complete the returned CompletableFuture
  * @param <U> the function's return type
  * @return the new CompletableFuture
  */
public static <U> CompletableFuture<U> supplyAsync(Supplier<U> supplier)
/**
  * @param supplier a function returning the value to be used
  * to complete the returned CompletableFuture
  * @param executor the executor to use for asynchronous execution
  * @param <U> the function's return type
  * @return the new CompletableFuture
  */
public static <U> CompletableFuture<U> supplyAsync(Supplier<U> supplier,
                                                       Executor executor)
```

案例：

未指定线程池

![image-20230221204142343](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221204142343.png)

![image-20230221204319085](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221204319085.png)

指定线程池：

![image-20230221204759078](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221204759078.png)

![image-20230221204839360](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221204839360.png)



#### 3.2.4 CompletableFuture异步编程

上边内容虽然使用的CompletableFuture，但还是用到了get方法，那不就跟 Future 一样了么？就类似下面的代码：

```java
 public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + " come in");
            int result = ThreadLocalRandom.current().nextInt(10);
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("5秒钟后的结果" + result);
            return result;
        });
        System.out.println(Thread.currentThread().getName()+" 先去忙其他的");
        System.out.println(completableFuture.get());
    }
```

虽然这一点证明了 CompletableFuture 完全可以替代 Future。但是 CompletableFuture 即实现了 Future 又实现了 CompletionStage 接口，按理来说应该会更强大，也就是说，==CompletableFuture是如何减少阻塞和轮询的==。

不使用get方法获取返回值，当异步任务完成时主动告诉我去获取结果。换句话说，Future能做的CompletableFuture 能做，Future不能做的CompletableFuture也能做。

Java8开始引入了CompletableFuture，它是Future的功能增强版，减少阻塞和轮询。可以传入回调对象，当异步任务完成或者发生异常时，自动调用回调对象的回调方法。

接下来我们演示一下：

![image-20230221214005333](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221214005333.png)

执行结果：

![image-20230221214058981](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221214058981.png)

可以看到，全程没有使用get()方法获取返回值，也照样拿到了。

注意：由于main线程相当于用户线程，而默认的线程池 ForkJoinPool 相当于 守护线程，当所有的用户线程(main)线程结束了，守护线程也会随着结束。所以这里最好使用 自定义线程池来规避主线程迅速结束导致守护线程迅速结束收不到结果。

![image-20230221214927994](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221214927994.png)

![image-20230221215005913](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221215005913.png)

### 3.3 CompletableFuture 的优点：

- 异步任务结束时，会自动回调某个对象的方法；
- 主线程设置好回调后，不再关心异步任务的执行，异步任务之间可以顺序执行；
- 异步任务出错时，对自动回调某个对象的方法；



## 4. 案例精讲：从电商网站的比价需求说开去

#### 4.1 函数式编程已成主流

##### 4.1.1 大厂面试题看看

![百度面试题](https://images-1313675740.cos.ap-shanghai.myqcloud.com/%E7%99%BE%E5%BA%A6%E9%9D%A2%E8%AF%95%E9%A2%98.png)

##### 4.1.2 Lambda表达式+Stream流式调用+Chain链式调用+Java8函数式编程

- **Runnable** ：我们已经说过很多次了，无参数，无返回值

```java
@FunctionalInterface
public interface Runnable {
    public abstract void run();
}
```

- **Function** ：功能型函数式接口；接受一个参数，并且有返回值

```java
@FunctionalInterface
public interface Function<T,R>{
	R apply(T t);
}
```

- **Consumer** ：消费型函数式接口；接受一个参数，没有返回值

```java
@FunctionalInterface
public interface Consumer<T>{
	void accept(T t);
}
```

- **BiConsumer** ：接受两个参数（Bi，英文单词词根，代表两个的意思），没有返回值

```java
@FunctionalInterface
public interface BiConsumer<T,U>{
	void accept(T t,U u);
}
```

- **Supplier** ：供给型函数式接口；没有参数，有一个返回值

```java
@FunctionalInterface
public interface Supplier<T>{
	T get(T t);
}
```

**小总结：**

![image-20230221223826595](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221223826595.png)

补充：链式调用

![image-20230221231143641](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230221231143641.png)

#### 4.2 先说说Join和get对比

![image-20230222103113560](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222103113560.png)

#### 4.3 说说你过去工作中的项目亮点

#### 4.4 大厂业务需求说明

功能——》性能

1. 需求说明

   同一款产品，同时搜索出同款产品在各大电商平台的售价；

   同一款产品，同时搜索出本产品在同一个电商平台下，各个入住卖家售价是多少

2. 输出返回

   出来结果希望是同款产品的在不同地方的价格清单列表，返回一个List<String>

   《mysql》in jd price is 88.05

   《mysql》 in dangdang price is 86.11

   《mysql》 in taobao price is 90.43

3. 解决方案

   step by step ，按部就班，查完京东查淘宝，查完淘宝查天猫......

   all in ，万箭齐发，一口气多线程异步任务同时查询......

#### 4.5 一波流Java8函数式编程带走-比价案例实战Case

```java
public class CompletableFutureMallDemo {
    static List<NetMall> list = Arrays.asList(
            new NetMall("jd"),
            new NetMall("dangdang"),
            new NetMall("taobao")
    );

    /**
     * step by step 一步一步查
     *
     * @param list
     * @param productName
     * @return
     */
    public static List<String> getPrice(List<NetMall> list, String productName) {
        // 流式调用
        return list
                .stream()
                .map(netMall -> String.format(productName + " in %s price is %.2f",
                        netMall.getNetMallName(),
                        netMall.calcPrice(productName)))
                .collect(Collectors.toList());
    }

    /**
     * all in  万剑齐发
     * 多线程异步任务
     *
     * @param list
     * @param productName
     * @return
     * List<NetMall> -----> List<CompletableFuture> ------> List<String>
     */
    public static List<String> getPriceByCompletableFuture(List<NetMall> list, String productName) {
        return list
                .stream()
                .map(netMall ->
                        CompletableFuture.supplyAsync(() ->
                                String.format(productName + " in %s price is %.2f",
                                        netMall.getNetMallName(),
                                        netMall.calcPrice(productName))))
                .collect(Collectors.toList())
                .stream()
                .map(s->s.join())
                .collect(Collectors.toList());

    }

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        List<String> list1 = getPrice(list, "mysql");
        for (String element : list1) {
            System.out.println(element);
        }
        long endTime = System.currentTimeMillis();
        System.out.println(" ---costTime " + (endTime - startTime) + " 毫秒");

        System.out.println("--------------------------");

        long startTime2 = System.currentTimeMillis();
        List<String> list2 = getPriceByCompletableFuture(list, "mysql");
        for (String element : list2) {
            System.out.println(element);
        }
        long endTime2 = System.currentTimeMillis();
        System.out.println(" ---costTime " + (endTime2 - startTime2) + " 毫秒");


    }
}

class NetMall {

    private String netMallName;

    public String getNetMallName() {
        return netMallName;
    }

    public NetMall(String netMallName) {
        this.netMallName = netMallName;
    }

    public double calcPrice(String productName) {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ThreadLocalRandom.current().nextDouble() * 2 + productName.charAt(0);
    }
}

```

![image-20230222125104903](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222125104903.png)

#### 4.6 CompletableFuture 常用方法

1. 获得结果和触发计算

   **获取结果：**

   ```java
   public T get() ：不见不散
   
   public T get(long timeout,TimeUnit unit) ：过时不候
   
   public T join() ：与 get() 一样，只不过编译不抛异常
   
   public T getNow(T valuelfAbsent) ：没有计算完成的情况下，给我一个替代结果。
   
   ​	立即获取结果不阻塞：计算完，返回计算完成后的结果；没算完，返回设定的valueIfAbsent值
   ```

   

   **主动触发计算：**

   ```java
   public boolean complete(T value) ：是否打断get方法立即返回括号值
   ```

   

2. 对计算结果进行处理

   **public <U> CompletableFuture<U> thenApply ：计算结果存在依赖关系，这两个线程串行化**

   ```java
   public class CompletableFutureAPIDemo2 {
       public static void main(String[] args) {
           ExecutorService threadPool = Executors.newFixedThreadPool(3);
           CompletableFuture.supplyAsync(() -> {
               try {
                   TimeUnit.SECONDS.sleep(2);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               System.out.println("111");
               return 1;
           },threadPool).thenApply(f -> {
               // 测试异常
               //int i = 10 / 0;
               System.out.println("222");
               return f + 2;
           }).thenApply(f -> {
               System.out.println("333");
               return f + 3;
           }).whenComplete((v, e) -> {
               if (e == null) {
                   System.out.println("计算结果值 " + v);
               }
           }).exceptionally(e -> {
               e.printStackTrace();
               System.out.println(e.getMessage()+"\t"+e.getCause());
               return null;
           });
           System.out.println("主线程去忙其他的了");
           threadPool.shutdown();
       }
   }
   ```

   ![image-20230222133716789](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222133716789.png)

   **异常相关：由于存在依赖关系(当前步错，不走下一步)，当前步骤有异常的话就叫停。**

   异常测试结果：第二步有异常，直接就停在了第二步

   ![image-20230222133951100](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222133951100.png)

   

   **handle ：计算结果存在依赖关系，这两个线程串行化**

   handle 正常处理与 thenApply一样。

   **异常相关 ：有异常也可以往下一步走，根据带的异常参数可以进一步处理。**

   ```java
   public static void main(String[] args) {
           //thenapply();
           ExecutorService threadPool = Executors.newFixedThreadPool(3);
           CompletableFuture.supplyAsync(() -> {
               try {
                   TimeUnit.SECONDS.sleep(2);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               System.out.println("111");
               return 1;
           }, threadPool).handle((f,e) -> {
               // 测试异常
               int i = 10 / 0;
               System.out.println("222");
               return f + 2;
           }).handle((f,e) -> {
               System.out.println("333");
               return f + 3;
           }).whenComplete((v, e) -> {
               if (e == null) {
                   System.out.println("计算结果值 " + v);
               }
           }).exceptionally(e -> {
               e.printStackTrace();
               System.out.println(e.getMessage() + "\t" + e.getCause());
               return null;
           });
   
           System.out.println("主线程去忙其他的了");
           threadPool.shutdown();
       }
   ```

   ![image-20230222135240492](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222135240492.png)

   **总结：**

   ![image-20230222135336222](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222135336222.png)

3. 对计算结果进行消费

   **thenAccept ：接受任务的处理结果，并消费处理，无返回结果**

   ```java
   public class CompletableFutureAPIDemo3 {
       public static void main(String[] args) {
           CompletableFuture.supplyAsync(() -> {
               return 1;
           }).thenApply(f -> {
               return f + 2;
           }).thenApply(f -> {
               return f + 3;
           }).thenAccept(System.out::println);
       }
   }
   ```

   ![image-20230222140950504](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222140950504.png)

   **对比补充 ：Code之任务之间的顺序执行**

   - thenRun(Runnable Runable)：任务A执行完执行任务B，并且B不要A的结果
   - thenAccept(Consumer action)：任务A执行完执行B，B需要A的结果，但是任务B==无返回值==
   - thenApply(Function fn)：任务A执行完执行B，B需要A的结果，同时任务B==有返回值==

   ```java
   public class CompletableFutureAPIDemo3 {
       public static void main(String[] args) {
           System.out.println(CompletableFuture.supplyAsync(() -> "resultA").thenRun(() -> {
           }).join());
   
           System.out.println(CompletableFuture.supplyAsync(() -> "resultA").thenAccept(r -> {
               System.out.println(r);
           }).join());
   
           System.out.println(CompletableFuture.supplyAsync(() -> "resultA").thenApply(f -> f + " resultB").join());
       }
   }
   ```

   ![image-20230222142738793](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222142738793.png)

4. 对计算速度进行选用

   谁快用谁

   `applyToEither()`：方法作用是返回两个任务中最先完成的任务结果

   Code：

   ![image-20230222160626723](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222160626723.png)

   ![image-20230222160709442](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222160709442.png)

   

5. 对计算结果进行合并

   `Combine()`

   两个CompleteStage任务都完成后，最终能把两个任务的结果一起交给thenCombine来处理，先完成的先等着，等待其他分支任务

   Code：

   ![image-20230222162739086](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222162739086.png)

   ![image-20230222162817153](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222162817153.png)

#### 4.7 CompletableFuture 和线程池说明

以`thenRun()` 和 `thenRunAsync` 为例，有什么区别？

**代码：**

不使用自定义线程池，`thenRun()` 与 `thenRunAsync()` 是一样的，都是使用默认线程池

![image-20230222150703001](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222150703001.png)

![image-20230222150805117](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222150805117.png)

使用自定义线程池，调用 `thenRun()` 方法，则都是使用自定义线程池

![image-20230222151922900](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222151922900.png)

使用自定义线程池，调用 `thenRunAsync()` 方法

![image-20230222152359786](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222152359786.png)

特殊情况：

![image-20230222153544903](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222153544903.png)

**小总结：**

1 没有传入自定义线程池，都用默认线程池ForkJoinPool；

2 传入了一个==自定义线程池==

如果你执行第一个任务的时候，传入了一个==自定义线程池==；

调用`thenRun` 方法执行第二个任务时，则第二个任务和第一个任务是共用同一个线程池。

调用`thenRunAsync` 执行第二个任务时，则第一个任务使用的是你自己传入的线程池，第二个任务使用的是ForkJoin线程池

3 备注

有可能处理太快，系统优化切换原则，直接使用main线程处理

其他如：`thenAccept`和`thenAcceptAsync`, `thenApply` 和 `thenApplyAsync` 等，他们之间的却别也是同理

**源码分析：**

![image-20230222154706729](https://images-1313675740.cos.ap-shanghai.myqcloud.com/image-20230222154706729.png)



















